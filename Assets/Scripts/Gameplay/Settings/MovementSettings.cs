
using UnityEngine;

[CreateAssetMenu(fileName = "MovementSettings", menuName = "Scriptable/Settings/Movement")]
public class MovementSettings : ScriptableObject
{
    [Header("Movement")]
    [SerializeField]
    private float _maxSpeed = 10.0f;
    [SerializeField]
    private float _maxAcceleration = 10.0f;
    [SerializeField]
    private float _maxAirAcceleration = 2.0f;
    [SerializeField]
    private float _jumpHeight = 2.0f;
    [SerializeField]
    private float _maxGroundAngle = 25.0f;
    [Header("Rotation")]
    private float _turnSpeed = 10f;
    [SerializeField]
    private float _turnSpeedMultiplier = 1.0f;

    public float MaxSpeed => _maxSpeed;
    public float MaxAcceleration => _maxAcceleration;
    public float MaxAirAcceleration => _maxAirAcceleration;
    public float JumpHeight => _jumpHeight;
    public float MaxGroundAngle => _maxGroundAngle;
    public float TurnSpeed => _turnSpeed;
    public float TurnSpeedMultiplier => _turnSpeedMultiplier;
}
