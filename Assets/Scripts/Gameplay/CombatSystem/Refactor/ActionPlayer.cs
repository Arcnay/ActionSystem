using System;
using System.Collections;
using UnityEngine;
using GuildKeys.Movement;

[Serializable]
public class ActionPlayer : MonoBehaviour
{
    [SerializeField]
    private AnimationController _animController;
    
    //Set from event that listens for "Grounded" Variable change in Movement component.
    [SerializeField]
    private bool _isGrounded;
    
    //Determined by Unsheathe/Sheathe animation events.
    [SerializeField]
    private bool _weaponInHand;
    
    //Receives from CancelWindow Animation Event
    public bool Cancellable;

    
    //Receive weapon data from equipment system (When equipped)
    [SerializeField]
    private Weapon _weapon;

    //DEBUG
    [SerializeField] private string NAction;
    [SerializeField] private string CAction;
    
    //Actions
    [SerializeField]
    private BaseActionList baseActionList;

    private AActionBase _nextAction;
    public AActionBase NextAction 
    { 
        get => _nextAction;
        set 
        { 
            _nextAction = value;
            NAction = value?.ActionName; print(NAction);
        }
    }

    private AActionBase _currentAction;
    public AActionBase CurrentAction
    {
        get => _currentAction;
        private set
        {
            _currentAction = value;
            if (_currentAction != null)
            {
                Cancellable = _currentAction.ActionType != EActionTypes.React;
            }
            CAction = value?.ActionName;
            print(CAction);
        }
    }
    
    //onEnable and onDisable
    private void OnEnable()
    {
        //TODO: Setup IController interface to Modulate the controller setup (Possess and Depossess)
        GetComponentInParent<PlayerController>().OnPerformedEvent += PerformInput;
        GetComponentInParent<PlayerController>().OnReleasedEvent += ReleaseInput;
        GetComponentInParent<MovementComponent3D>().OnIsGrounded += SetGrounded;
    }

    private void OnDisable()
    {
        // TODO: Setup Unsubscribe through Disable.
        //  GetComponentInParent<PlayerController>().OnPerformedEvent -= PerformInput;
        //  GetComponentInParent<PlayerController>().OnReleasedEvent -= ReleaseInput;
        //  GetComponentInParent<MovementComponent3D>().OnIsGrounded -= SetGrounded;
    }
    
    private void SetGrounded(bool grounded) => _isGrounded = grounded;

    private void PerformInput(EInputKey input) => SetNextAction(TryGetAction(TrySetAerial(input)));
    
    private void ReleaseInput(EInputKey input) => EndAction();

    private EInputKey TrySetAerial(EInputKey input) => !_isGrounded ? input switch 
        {
            EInputKey.Primary => EInputKey.AerialPrimary,
            EInputKey.Secondary => EInputKey.AerialSecondary,
            _ => input
        }
        : input;

    private AActionBase TryGetAction(EInputKey input)
    {
        if (!_weaponInHand) return baseActionList?.ActionCommandList.CheckCommandList(input, this);

        if (CurrentAction is AttackAction)
        {
            AttackAction _attackAction = (AttackAction) CurrentAction;
            return _attackAction?.ActionList.CheckCommandList(input, this);
        }
        return _weapon?.ActionList.CheckCommandList(input, this);
    }

    public void SetNextAction(AActionBase action)
    {
        NextAction = action;
        if (NextAction == null) return;
        
        if (CurrentAction == null)
        {
            StartAction();
            return;
        }
        
        if((NextAction?.ActionType == EActionTypes.React) || (NextAction.CanCancel && Cancellable))
        {
            CancelAction();
        }
    }

    public void StartAction()
    {
        print(NextAction.ActionName);
        CurrentAction = NextAction;
        NextAction = null;
        
        //TODO: Send data to animator or anim controller
        //TODO: Send movement information to MovementComponent (Such as curve, movement mode, etc.)
        
        CurrentAction?.OnActionStart();

        //DEBUG Triggers without animations
        if (_animController == null)
        {
            //DebugCoroutine();
        }
    }
    
    private void EndAction()
    {
        CurrentAction?.OnActionEnd();
    }

    public void CancelAction()
    {
        CurrentAction?.OnActionCanceled();
        CurrentAction = null;
        //TODO: Stop Animator from playing specific action.
        //TODO: Free up movement settings?
        StartAction();
    }

#region DEBUG SECTION

    private void DebugCoroutine()
    {
        StopCoroutine(ActionTimer());
        if (CurrentAction != null)
        {
            StartCoroutine(ActionTimer());
        }
    }
    
    private IEnumerator ActionTimer()
    {
        yield return new WaitForSeconds(2);
        if (NextAction != null)
        {
            StartAction();
            yield break;
        }
        CancelAction();
    }
    
#endregion

public event Action OnUpdate = delegate {  };
    private void Update()
    {
        if (_currentAction?.ActionType == EActionTypes.Charge)
        {
            OnUpdate();
        }
    }
}
