using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public abstract class AActionBase
{
    public string ActionName;
    public EActionTypes ActionType;
    public bool CanMove;
    public bool CanCancel;

    public float TransitionOutTime = 0.1f;
    protected ActionPlayer _owner;
    
    public abstract void OnActionStart();

    public abstract void OnActionEnd();

    public abstract void OnActionCanceled();
}

public abstract class ScriptableAction : ScriptableObject
{
    public abstract AActionBase GetAction(ActionPlayer owner);
}