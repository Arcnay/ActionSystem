using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class CommandList
{
    [SerializeField]
    private List<EInputKey> _inputList = new List<EInputKey>();
    [SerializeField]
    private List<ScriptableAction> _actionList = new List<ScriptableAction>();
    private Dictionary<EInputKey, ScriptableAction> _commandList = new Dictionary<EInputKey, ScriptableAction>();

    private void OnEnable()
    {
        SetCommandList();
    }

    public void SetCommandList()
    {
        _commandList.Clear();
        foreach (EInputKey item in _inputList)
        {
            if (_inputList.Count == 0 || _actionList.Count == 0)
            {
                break;
            }
            _commandList.Add(item, _actionList[ _inputList.IndexOf(item) ]);
        }
    }

    public AActionBase CheckCommandList(EInputKey input, ActionPlayer owner) => 
        _commandList.TryGetValue(input, out ScriptableAction action) ? action.GetAction(owner) : null;
}
