using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

[CreateAssetMenu(fileName = "AttackAction", menuName = "ScriptableObjects/Actions/AttackAction", order = 1)]
public class AttackAction_SA : ScriptableAction
{
    public CommandList ActionList;
    
    [SerializeField]
    private HitData _hitData;
    [SerializeField]
    private CollisionData _collisionData;
    [SerializeField]
    private VfxData _vfxData;
    
    private void OnEnable() => ActionList.SetCommandList();
    
    public override AActionBase GetAction(ActionPlayer owner)
    {
        return new AttackAction(this.name, owner, ActionList, _hitData, _collisionData, _vfxData);
    }
}

public class AttackAction : AActionBase
{
    
    public CommandList ActionList;
    private HitData _hitData;
    private CollisionData _collisionData;
    private VfxData _vfxData;

    public AttackAction(string actionName, ActionPlayer owner, CommandList actionList, HitData hitData, CollisionData collisionData, VfxData vfxData)
    {
        ActionName = actionName;
        ActionType = EActionTypes.Attack;
        _owner = owner;
        
        ActionList = actionList;
        _hitData = hitData;
        _collisionData = collisionData;
        _vfxData = vfxData;
    }
    

    public override void OnActionStart()
    {
        
    }

    public override void OnActionEnd()
    {
        
    }

    public override void OnActionCanceled()
    {
        
    }
    
    //TODO: Revisit this code
    public void CastForTargets()
    {
        Vector3 location = DetermineOffset();
        Collider[] hitTargets = { };
        var size = Physics.OverlapSphereNonAlloc(location, _collisionData.Radius, hitTargets);
        foreach (Collider target in hitTargets)
        {
            GameObject targetObject = target.gameObject;
            bool targetObjectIsOwner = targetObject != _owner.gameObject;
            bool targetObjectHasCharacter = targetObject.GetComponent<Character>() != null;

            if (targetObjectIsOwner && targetObjectHasCharacter)
            {
                float angle = Vector3.Angle(_owner.transform.position, targetObject.transform.position);
                if (Mathf.Abs(angle) < _collisionData.HitAngle)
                {
                    IDamagable damagableComponent = targetObject.GetComponent<IDamagable>();
                    //damagableComponent?.OnHit(_hitData, owner.Weapon.AttackValue);
                }
            }
        }
    }

    public void AttackVFX()
    {
        if (_vfxData.AttackVFX == null) return;
        GameObject vfx = Object.Instantiate(_vfxData.AttackVFX, DetermineOffset(), DetermineRotation());
        vfx.transform.localScale *= _collisionData.Radius;
    }
    private Quaternion DetermineRotation()
    {
        var transform = _owner.transform;
        var rotation = Quaternion.LookRotation(transform.forward, transform.up);
        rotation *= Quaternion.Euler(_vfxData.VfxRotation);
        return rotation;
    }
    private Vector3 DetermineOffset()
    {
        var transform = _owner.transform;
        Vector3 xOffset = _collisionData.LocationOffset.x * transform.right;
        Vector3 yOffset = _collisionData.LocationOffset.y * transform.up;
        Vector3 zOffset = _collisionData.LocationOffset.z * transform.forward;
        Vector3 offset = xOffset + yOffset + zOffset;
        Vector3 offsetLocation = transform.position + offset;
        return offsetLocation;
    }
}

[Serializable]
public struct HitData
{
    [Range(0, 100)]
    public float MotionValue;
    public EPowerLevel KnockType;
    public GameObject HitVFX;
}

[Serializable]
public struct VfxData
{
    public GameObject AttackVFX;
    public Vector3 VfxRotation;
}

[Serializable]
public struct CollisionData
{
    public Vector3 LocationOffset;
    public float Radius;
    public float HitAngle;
}
