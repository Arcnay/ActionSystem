using System;
using UnityEngine;

[CreateAssetMenu(fileName = "ChargeAction", menuName = "ScriptableObjects/Actions/ChargeAction", order = 1)]
public class ChargeAction_SA : ScriptableAction
{
    [SerializeField] private int _chargeLevel;
    [SerializeField] private float _chargeRate;
    [SerializeField] private float _chargeMax;
    //Tooltip: At what % do are these triggered.
    [Range(0, 100)]
    [SerializeField] private int[] _thresholds;
    [SerializeField] private ScriptableAction[] _actions;

    public override AActionBase GetAction(ActionPlayer owner)
    {
        return new ChargeAction(this.name, owner, _chargeLevel, _chargeRate, _chargeMax, _thresholds, _actions);
    }
}

public class ChargeAction : AActionBase
{
    private int _chargeLevel;
    private float _chargeRate;
    private float _chargeMax;
    private int[] _thresholds;
    private ScriptableAction[] _actions;
    
    private float _holdDuration;
    
    public ChargeAction(string actionName, ActionPlayer owner, int chargeLevel, float chargeRate, 
        float chargeMax, int[] thresholds, ScriptableAction[] actions)
    {
        ActionName = actionName;
        ActionType = EActionTypes.Charge;
        _owner = owner;
        
        _chargeLevel = chargeLevel;
        //_chargeRate = chargeRate; unused atm
        //_chargeMax = chargeMax; unused atm
        //Tooltip: At what % do are these triggered.
        _thresholds = thresholds;
        _actions = actions;
    }
    
    public override void OnActionStart()
    {
        Reset();
        Debug.Log("Charging");
        _owner.OnUpdate += TickTimer;
    }

    public override void OnActionEnd()
    {
        _owner.OnUpdate -= TickTimer;
        _owner.NextAction = ReturnAction();
        _owner.CancelAction();
    }

    public override void OnActionCanceled()
    {
        _owner.OnUpdate -= TickTimer;
    }

    //Timer Section
    
    private void Reset()
    {
        _holdDuration = 0;
        _chargeLevel = 0;
    }

    private void TickTimer()
    {
        _holdDuration += Time.deltaTime;
        var chargeAmount = (int)Math.Floor(_holdDuration);
        CheckThreshold(chargeAmount);
    }

    private void CheckThreshold(int chargeAmount)
    {
        if (_chargeLevel + 1 >= _thresholds.Length) return;
        
        if (chargeAmount == _thresholds[_chargeLevel + 1])
        {
            _chargeLevel++;
            Debug.Log(_chargeLevel);
        }
    }

    private AActionBase ReturnAction()
    {
        if (_chargeLevel + 1 > _thresholds.Length) return null;
        ScriptableAction action = _actions[_chargeLevel] == null ? null : _actions[_chargeLevel];
        Debug.Log(action?.GetAction(_owner));
        return action?.GetAction(_owner);
    }
}
