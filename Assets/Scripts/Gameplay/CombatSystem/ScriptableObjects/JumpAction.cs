using UnityEngine;

public class Jump : AActionBase
{
    //private ChargeActionTimer _timer;
    public Jump(ActionPlayer owner)
    {
        base._owner = owner;
    }
    public override void OnActionCanceled()
    {
        //_timer.HoldActive = false;
    }
    public override void OnActionEnd()
    {
        //_timer.HoldActive = false;
    }
    public override void OnActionStart()
    {
        //_timer = new //ChargeActionTimer
        {
            //HoldActive = true
        };
    }

    public void TickTimer()
    {
        //_timer.TickTimer();
    }
}

[CreateAssetMenu(fileName = "Jump", menuName = "ScriptableObjects/Actions/Jump", order = 1)]
public class JumpAction : ScriptableAction
{
    public override AActionBase GetAction(ActionPlayer owner)
    {
        return new Jump(owner);
    }
    
}