using UnityEngine;

[CreateAssetMenu(fileName = "ReactAction", menuName = "ScriptableObjects/Actions/HitReact", order = 2)]
public class HitReactAction_SA : ScriptableAction
{
    public override AActionBase GetAction(ActionPlayer owner)
    {
        return new HitReactAction(owner);
    }
}

public class HitReactAction : AActionBase
{
    public HitReactAction(ActionPlayer owner)
    {
        ActionType = EActionTypes.React;
        CanCancel = true;
        _owner = owner;
    }
    public override void OnActionStart()
    {
    }

    public override void OnActionEnd()
    {
    }

    public override void OnActionCanceled()
    {
    }
}
