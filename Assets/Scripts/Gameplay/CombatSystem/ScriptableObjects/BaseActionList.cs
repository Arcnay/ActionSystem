using System;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterBaseActions", menuName = "ScriptableObjects/Character/CharacterBaseActions", order = 0)]
public class BaseActionList : ScriptableObject
{
    public CommandList ActionCommandList;
    
    [SerializeField]
    private HitReactAction[] _reactList = new HitReactAction[ Enum.GetNames(typeof(EPowerLevel)).Length ];

    private void OnEnable() => ActionCommandList.SetCommandList();

    public HitReactAction GetHitReactAction(EPowerLevel hitLevel)
    {
        return _reactList[(int) hitLevel];
    }
}
