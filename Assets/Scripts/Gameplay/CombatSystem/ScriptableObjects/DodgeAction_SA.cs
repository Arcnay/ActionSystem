using UnityEngine;

[CreateAssetMenu(fileName = "Dodge", menuName = "ScriptableObjects/Actions/Dodge", order = 2)]
public class DodgeAction_SA : ScriptableAction
{
    public override AActionBase GetAction(ActionPlayer owner)
    {
        return new DodgeAction(owner);
    }
}

public class DodgeAction : AActionBase
{
    public DodgeAction(ActionPlayer owner)
    {
        _owner = owner;
        ActionType = EActionTypes.Defense;
    }
    public override void OnActionCanceled()
    {
    }

    public override void OnActionEnd()
    {
    }

    public override void OnActionStart()
    {
    }
}
