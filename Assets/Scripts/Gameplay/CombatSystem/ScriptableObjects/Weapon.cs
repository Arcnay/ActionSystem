using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Objects/Weapon", order = 2)]
public class Weapon : ScriptableObject
{
    public EWeaponType WeaponType = 0;
    [SerializeField]
    private GameObject _prefab;
    public float AttackValue = 0;
    public CommandList ActionList;
    private void OnEnable() => ActionList.SetCommandList();
}
