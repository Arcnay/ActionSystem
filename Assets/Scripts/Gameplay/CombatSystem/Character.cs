using UnityEngine;
using GuildKeys.Movement;

public class Character : MonoBehaviour, IDamagable
{
    [SerializeField]
    private BaseActionList baseActionList = null;
    public Weapon Weapon;
    public AActionBase CurrentAction = null;
    public AActionBase NextAction = null;

    public Animator Animator = null;
    [SerializeField]
    private int _ignoreAnimLayers = 1;
    public MovementComponent3D Movement = null;

    private void Awake()
    {
        Animator = GetComponentInChildren<Animator>();
        Movement = GetComponent<MovementComponent3D>();
    }
    
    public AActionBase TryGetAction(EInputKey input)
    {
        AActionBase action = null;
        // if (CurrentAction?.ActionType == EActionTypes.React)
        // {
        //     return action;
        // }
        //
        // if (CurrentAction != null)
        // {
        //     action = (AttackAction) CurrentAction;
        //     //action = action.ActionList.CheckCommandList(input);
        //     return action;
        // }
        // else
        // {
        //     action = Weapon.ActionCommandList.CheckCommandList(input);
        // }
        //
        // if (action == null)
        // {
        //     action = baseActionList.ActionCommandList.CheckCommandList(input);
        // }

        return action;
    }

    public void SetNextAction(AttackAction action)
    {
        NextAction = action;
        if (CurrentAction?.ActionType != EActionTypes.Defense)
        {
            if (NextAction?.ActionType == EActionTypes.React || CurrentAction == null)
            {
                CancelAction(CurrentAction, NextAction);
            }
        }
    }

    public void StartAction(AActionBase action)
    {
        CurrentAction = action;

        if (action != null)
        {
            Movement.CanMove = CurrentAction.CanMove;
            int layer = action.ActionType != EActionTypes.Attack ? 0 : (int) Weapon.WeaponType + _ignoreAnimLayers;
            Animator.CrossFade(action.ToString(), action.TransitionOutTime, layer);
            //action.OnActionStart(this);
        }

        NextAction = null;
    }

    //public void EndAction(AActionBase action) => action?.OnActionEnd(this);

    public void CancelAction(AActionBase action, AActionBase nextAction)
    {
        CurrentAction = null;
        Movement.CanMove = true;
        Animator.Play("Empty", (int) Weapon.WeaponType + _ignoreAnimLayers);
        //action?.OnActionCanceled(this);
        StartAction(nextAction);
    }

    #region Hit function

    public void OnHit(HitData hitData, float attackValue)
    {
        GetHitReact(hitData.KnockType);
        SpawnHitVFX(hitData);
        ApplyDamage(DamageCalculation(attackValue, hitData.MotionValue));
    }

    private void GetHitReact(EPowerLevel powerLevel)
    {
        if (baseActionList?.GetHitReactAction(powerLevel)?.ToString() == "Flinch")
        {
            Animator.Play("Flinch", _ignoreAnimLayers);
        }
        else
        {
            CancelAction(CurrentAction, baseActionList?.GetHitReactAction(powerLevel));
        }
    }

    private float DamageCalculation(float attackValue, float motionValue)
    {
        float dmgResult = attackValue * (motionValue / 100);
        return dmgResult;
    }

    private void ApplyDamage(float damage)
    {
        //TODO: Apply damage to health when Health is integrated.
        print(damage);
        return;
    }

    #endregion

    #region Hit VFX Implementation

    private void SpawnHitVFX(HitData hitData)
    {
        CapsuleCollider collider = GetComponent<CapsuleCollider>();
        _ = Instantiate(hitData.HitVFX, collider.bounds.center, Random.rotation);
        print(collider.center);
    }

    #endregion
}
