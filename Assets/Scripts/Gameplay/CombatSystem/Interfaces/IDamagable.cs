using UnityEngine;

public interface IDamagable
{
    void OnHit(HitData hitData, float attackValue);
}
