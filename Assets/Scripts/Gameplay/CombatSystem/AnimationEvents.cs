using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    private ActionPlayer _owner;

    private void Awake()
    {
        _owner = GetComponentInParent<ActionPlayer>();
    }

    public void CloseBuffer()
    {
        if (_owner.NextAction != null)
        {
            _owner.StartAction();
        }
    }

    public void ResetBuffer() => _owner.CancelAction();

    public void TriggerCast()
    {
        //TODO: Clean up Type code.
        var action = (AttackAction) _owner.CurrentAction;
        action.CastForTargets();
        
    }

    public void TriggerVFX()
    {
        var action = (AttackAction) _owner.CurrentAction;
        action.AttackVFX();
    }

    public void ToggleCancellable()
    {
        _owner.Cancellable = !_owner.Cancellable;
    }
}
