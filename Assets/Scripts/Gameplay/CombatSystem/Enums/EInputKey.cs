public enum EInputKey
{
    None,
    Primary,
    Secondary,
    Dodge,
    Jump,
    AerialPrimary,
    AerialSecondary
}
