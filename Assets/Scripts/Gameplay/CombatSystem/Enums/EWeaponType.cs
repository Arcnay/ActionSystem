public enum EWeaponType
{
    Unarmed,
    Sword,
    Daggers,
    Bow,
    Hammer
}
