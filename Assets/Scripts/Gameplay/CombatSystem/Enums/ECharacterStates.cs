public enum ECharacterStates
{
    Idle,
    Moving,
    Jumping,
    Action,
    Interacting,
    Dodging,
    Hit,
    Stun,
    KnockedDown,
    Downed,
    Dead
}
