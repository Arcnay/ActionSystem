using UnityEngine;
using UnityEngine.InputSystem;
using System;
using static PlayerInputActions;

namespace GuildKeys.Input
{
    [CreateAssetMenu(fileName = "InputChannel", menuName = "Scriptable/Channels/Input")]
    public class InputChannel : ScriptableObject, IPlayerActions
    {
        private PlayerInputActions _actions = null;

        public event Action<Vector2> OnMoveEvent;
        public event Action<Vector2> OnLookEvent;
        public event Action<InputAction.CallbackContext> OnPrimaryEvent;
        public event Action<InputAction.CallbackContext> OnSecondaryEvent;
        public event Action<InputAction.CallbackContext> OnDodgeEvent;
        public event Action<InputAction.CallbackContext> OnJumpEvent;

        private void OnEnable()
        {
            if (_actions == null)
            {
                _actions = new PlayerInputActions();
                _actions.Player.SetCallbacks(this);
            }

            EnableInput();
        }

        private void OnDisable() => _actions.Player.Disable();

        private void EnableInput() => _actions.Player.Enable();

        void IPlayerActions.OnMove(InputAction.CallbackContext context)
        {
            Vector2 value = context.ReadValue<Vector2>();
            OnMoveEvent?.Invoke(value);
        }

        public void OnLook(InputAction.CallbackContext context)
        {
            Vector2 value = context.ReadValue<Vector2>();
            OnLookEvent?.Invoke(value);
        }

        public void OnPrimary(InputAction.CallbackContext context) => OnPrimaryEvent?.Invoke(context);

        public void OnSecondary(InputAction.CallbackContext context) => OnSecondaryEvent?.Invoke(context);

        public void OnDodge(InputAction.CallbackContext context) => OnDodgeEvent?.Invoke(context);
        public void OnJump(InputAction.CallbackContext context) => OnJumpEvent?.Invoke(context);
    }
}
