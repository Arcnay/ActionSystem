using UnityEngine;
using GuildKeys.Input;
using System;

namespace GuildKeys.Movement
{
    [RequireComponent(typeof(Rigidbody))]
    public class MovementComponent3D : MonoBehaviour
    {
        [SerializeField]
        private MovementSettings _settings = null;

        private Vector3 _velocity = Vector3.zero;
        private Vector3 _desiredVelocity = Vector3.zero;
        private Rigidbody _body = null;
        private bool _desiredJump = false;
        private float _minGroundDotProduct = 0.0f;
        private Vector3 _contactNormal = Vector3.zero;
        private int _groundContactCount = 0;

        public Vector3 MovementInput { get; private set; }
        private Vector3 DesiredVelocity { set => _desiredVelocity = value * _settings.MaxSpeed; }
        
        private bool _onGround;
        public bool OnGround
        {
            get
            {
                if (_onGround == _groundContactCount > 0) return _onGround;
                _onGround = _groundContactCount > 0;
                OnIsGrounded?.Invoke(_onGround);
                return _onGround;
            }
        }


        public event Action<bool> OnIsGrounded;
        public Vector3 Velocity => _body.velocity;

        [HideInInspector]
        public bool CanMove = true;


        private void OnValidate() => _minGroundDotProduct = Mathf.Cos(_settings.MaxGroundAngle * Mathf.Deg2Rad);

        private void Awake()
        {
            _body = GetComponent<Rigidbody>();
            OnValidate();
        }

        private void printString(bool variable) => Debug.Log(variable);

        private void FixedUpdate()
        {
            UpdateState();
            AdjustVelocity();

            if (_desiredJump == true)
            {
                _desiredJump = false;
                Jump();
            }
            _body.velocity = _velocity;

            ClearState();
        }

        public void OnInputReceived(Vector2 value)
        {
            var playerInput = Vector2.ClampMagnitude(value, 1.0f);
            MovementInput = (Vector3.right * playerInput.x) + (Vector3.forward * playerInput.y);
            DesiredVelocity = CanMove ? MovementInput : Vector3.zero;
        }

        private void ClearState()
        {
            _groundContactCount = 0;
            _contactNormal = Vector3.zero;
        }

        private void UpdateState()
        {
            _velocity = _body.velocity;
            if (OnGround == true)
            {
                if (_groundContactCount > 1)
                {
                    _contactNormal.Normalize();
                }
            }
            else
            {
                _contactNormal = Vector3.up;
            }
        }

        private Vector3 ProjectOnContactPlane(Vector3 vector)
        {
            Vector3 perpendicularVector = _contactNormal * Vector3.Dot(vector, _contactNormal);

            return vector - perpendicularVector;
        }

        private void AdjustVelocity()
        {
            Vector3 xAxis = ProjectOnContactPlane(Vector3.right).normalized;
            Vector3 zAxis = ProjectOnContactPlane(Vector3.forward).normalized;

            float currentX = Vector3.Dot(_velocity, xAxis);
            float currentZ = Vector3.Dot(_velocity, zAxis);

            float acceleration = OnGround ? _settings.MaxAcceleration : _settings.MaxAirAcceleration;
            float maxSpeedChange = acceleration * Time.deltaTime;

            float newX = Mathf.MoveTowards(currentX, _desiredVelocity.x, maxSpeedChange);
            float newZ = Mathf.MoveTowards(currentZ, _desiredVelocity.z, maxSpeedChange);

            _velocity += (xAxis * (newX - currentX)) + (zAxis * (newZ - currentZ));
        }

        private void Jump()
        {
            if (OnGround == true)
            {
                float jumpSpeed = Mathf.Sqrt(-2.0f * Physics.gravity.y * _settings.JumpHeight);
                float alignedSpeed = Vector3.Dot(_velocity, _contactNormal);

                if (alignedSpeed > 0.0f)
                {
                    jumpSpeed = Mathf.Max(jumpSpeed - alignedSpeed, 0.0f);
                }
                _velocity += _contactNormal * jumpSpeed;
            }
        }

        private void OnCollisionEnter(Collision other) => EvaluateCollision(other);

        private void OnCollisionStay(Collision other) => EvaluateCollision(other);

        private void EvaluateCollision(Collision other)
        {
            for (int index = 0; index < other.contactCount; index++)
            {
                Vector3 normal = other.GetContact(index).normal;
                if (normal.y >= _minGroundDotProduct)
                {
                    _groundContactCount += 1;
                    _contactNormal += normal;
                }
            }
        }
    }
}
