using GuildKeys.Input;

using UnityEngine;

namespace GuildKeys.Movement
{
    [RequireComponent(typeof(Rigidbody))]
    public class RotationComponent3D : MonoBehaviour
    {
        [SerializeField]
        private MovementSettings _settings = null;
        private Vector3 _lookingDirection = Vector3.forward;
        private Rigidbody _body = null;

        private void Awake() => _body = GetComponent<Rigidbody>();

        public void SetLookingDirection(Vector2 val)
        {
            Vector3 newLookingDirection = (Vector3.right * val.x) + (Vector3.forward * val.y);
            if (newLookingDirection.normalized.magnitude < 0.05f)
            {
                return;
            }
            _lookingDirection = newLookingDirection;
        }

        private void FixedUpdate() => Rotate();

        private void Rotate()
        {
            float rotationRatio = Time.fixedDeltaTime * _settings.TurnSpeed * _settings.TurnSpeedMultiplier;
            var targetRotation = Quaternion.LookRotation(_lookingDirection);
            var rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationRatio);
            
            _body.MoveRotation(rotation);
        }
    }
}
