using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

[Serializable]
public class Attribute
{
    public float BaseValue;

    protected float lastBaseValue = float.MinValue;
    protected bool isDirty = true;
    protected float _value;

    public virtual float Value { get 
        { 
            if (isDirty || BaseValue != lastBaseValue)
            {
                lastBaseValue = BaseValue;
                _value = CalculateFinalValue();
                isDirty = false;
            }
            return _value;
        } 
    }

    protected readonly List<AttributeModifier> statModifiers;
    public readonly ReadOnlyCollection<AttributeModifier> Statmodifiers;

    public Attribute()
    {
        statModifiers = new List<AttributeModifier>();
        Statmodifiers = statModifiers.AsReadOnly();
    }
    public Attribute(float baseValue) : this ()
    {
        BaseValue = baseValue;
    }

    public virtual void AddModifier(AttributeModifier mod)
    {
        isDirty = true;
        statModifiers.Add(mod);
        statModifiers.Sort(CompareOrder);
    }

    protected virtual int CompareOrder(AttributeModifier a, AttributeModifier b)
    {
        if(a.Order < b.Order)
            return -1;
        else if (a.Order > b.Order)
            return 1;
        return 0; //if a.order == b.order
    }

    public virtual bool RemoveModifier(AttributeModifier mod)
    {
        if(statModifiers.Remove(mod))
        {
            isDirty = true;
            return true;
        }
        return false;
    }

    public virtual bool RemoveAllModsFromSource(object source)
    {
        bool didRemove = false;
        for (int i = statModifiers.Count - 1; i >= 0; i--)
        {
            if (statModifiers[i].Source == source)
            {
                isDirty = true;
                didRemove = true;
                statModifiers.RemoveAt(i);
            }
        }
        return didRemove;
    }

    protected virtual float CalculateFinalValue()
    {
        float finalValue = BaseValue;
        float sumPercentAdd = 0;

        for (int i = 0; i < statModifiers.Count; i++)
        {
            AttributeModifier mod = statModifiers[i];

            if (mod.Type == AttrModType.Flat)
            {
                finalValue += mod.Value;
            }
            else if (mod.Type == AttrModType.PercentAdd)
            {
                sumPercentAdd += mod.Value;

                if (i + 1 >= statModifiers.Count || statModifiers[i + 1].Type != AttrModType.PercentAdd)
                {
                    finalValue += 1 + sumPercentAdd;
                    sumPercentAdd = 0;
                }
            }
            else if (mod.Type == AttrModType.PercentMultiply)
            {
                finalValue *= 1 + mod.Value;
            }
        }

        return (float)Math.Round(finalValue, 4);
    }
}

public enum AttrModType
{
    Flat = 100,
    PercentAdd = 200,
    PercentMultiply = 300
}
