public class AttributeModifier
{
    public readonly float Value;
    public readonly AttrModType Type;
    public readonly int Order;
    public readonly object Source;
 
    public AttributeModifier(float value, AttrModType type, int order, object source)
    {
        Value = value;
        Type = type;
        Order = order;
        Source = source;
    }

    public AttributeModifier(float value, AttrModType type) : this (value,type,(int)type, null) {}
    public AttributeModifier(float value, AttrModType type, int order) : this (value,type,order, null) {}
    public AttributeModifier(float value, AttrModType type, object source) : this (value,type,(int)type, source) {}
}
