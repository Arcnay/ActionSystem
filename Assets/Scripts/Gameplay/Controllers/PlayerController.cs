using UnityEngine;
using UnityEngine.InputSystem;
using GuildKeys.Input;
using GuildKeys.Movement;
using Cinemachine;
using Vocario.Scriptable;
using System;

#pragma warning disable 649

public class PlayerController : MonoBehaviour
{
    [Header("Input")]
    [SerializeField]
    private InputChannel _inputChannel = null;
    [Header("Cameras")]
    [SerializeField]
    private CinemachineFreeLook _cinemachineCamera = null;
    [SerializeField]
    private RefCamera _mainCamera = null;
    private MovementComponent3D _characterMovement = null;
    private RotationComponent3D _rotationComponent = null;

    //Combat system
    private EInputKey _pressedInput;
    private Character _character;
    private Vector2 _moveInput = Vector2.zero;

    private void Awake() => Possess();

    private void OnDestroy() => Depossess();

    public void Possess()
    {
        _characterMovement = GetComponent<MovementComponent3D>();
        _rotationComponent = GetComponent<RotationComponent3D>();
        _character = GetComponent<Character>();
        Cursor.visible = false;
        _inputChannel.OnMoveEvent += SetMoveInput;
        _inputChannel.OnLookEvent += Look;
        _inputChannel.OnPrimaryEvent += OnPrimary;
        _inputChannel.OnSecondaryEvent += OnSecondary;
        _inputChannel.OnDodgeEvent += OnDodge;
        //_inputChannel.OnJumpEvent += OnJump;
    }

    public void Depossess()
    {
        _characterMovement = null;
        _rotationComponent = null;
        _character = null;
        Cursor.visible = true;
        _inputChannel.OnMoveEvent -= SetMoveInput;
        _inputChannel.OnLookEvent -= Look;
        _inputChannel.OnPrimaryEvent -= OnPrimary;
        _inputChannel.OnSecondaryEvent -= OnSecondary;
        _inputChannel.OnDodgeEvent -= OnDodge;
        //_inputChannel.OnJumpEvent -= OnJump;
    }

    private void Update() => Move();

    #region Handle Inputs

    private void Move()
    {
        Vector3 up = Vector3.up;
        Vector3 right = _mainCamera.Value.transform.right;
        var forward = Vector3.Cross(right, up);
        Vector3 convertedInput = (forward * _moveInput.y) + (right * _moveInput.x);
        Vector2 moveInput = (Vector2.right * convertedInput.x) + (Vector2.up * convertedInput.z);

        _characterMovement.OnInputReceived(moveInput);
        _rotationComponent.SetLookingDirection(moveInput);
    }

    private void SetMoveInput(Vector2 val) => _moveInput = val;

    private void Look(Vector2 val)
    {
        if (_cinemachineCamera == null)
        {
            return;
        }
        _cinemachineCamera.m_XAxis.m_InputAxisValue = val.x;
        _cinemachineCamera.m_YAxis.m_InputAxisValue = -val.y;
    }


    private void OnDodge(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnPerformed(EInputKey.Dodge);
        }
    }

    private void OnPrimary(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnPerformed(EInputKey.Primary);
        }

        if (context.canceled)
        {
            OnCanceled(EInputKey.Primary);
        }
    }

    private void OnSecondary(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            OnPerformed(EInputKey.Secondary);
        }

        if (context.canceled)
        {
            OnCanceled(EInputKey.Secondary);
        }
    }

    #endregion

    public event Action<EInputKey> OnPerformedEvent;
    public event Action<EInputKey> OnReleasedEvent;

    private void OnPerformed(EInputKey input)
    {
        OnPerformedEvent?.Invoke(input);
    }

    private void OnCanceled(EInputKey input)
    {
        OnReleasedEvent?.Invoke(input);
    }

    
}
