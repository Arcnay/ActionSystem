﻿using UnityEngine;
using GuildKeys.Movement;

public class AnimationController : MonoBehaviour
{
    [SerializeField]
    private float _dampTime = 0.1f;
    [SerializeField]
    private MovementSettings _settings = null;
    [SerializeField] 
    private int _ignoreAnimLayers;
    private Animator _animator;
    private MovementComponent3D _characterMovement;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _characterMovement = GetComponentInParent<MovementComponent3D>();
    }

    private void Update()
    {
        Vector3 rightVector = Vector3.right * _characterMovement.Velocity.x;
        Vector3 forwardVector = Vector3.forward * _characterMovement.Velocity.z;
        Vector3 cleanVelocityVector = rightVector + forwardVector;

        float maxValue = cleanVelocityVector.magnitude / _settings.MaxSpeed;
        float speed = Mathf.Min(_characterMovement.MovementInput.magnitude, maxValue);

        _animator.SetFloat("Speed", speed, _dampTime, Time.deltaTime);
        _animator.SetBool("IsGrounded", _characterMovement.OnGround);
    }

    public void PlayAnimation(AActionBase _action, Weapon _weapon)
    {
        int layer = _action.ActionType != EActionTypes.Attack ? 0 : (int) _weapon.WeaponType + _ignoreAnimLayers;
        _animator.CrossFade(_action.ToString(), _action.TransitionOutTime, layer);
    }

    
    /*
     * Receive action information from Actionplayer
     * Listens to weapon equip/unequip event<bool>
     * controls all things related to the Mecanim animator.
     */
}
