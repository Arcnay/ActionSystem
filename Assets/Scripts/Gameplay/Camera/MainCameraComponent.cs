using Vocario.Scriptable;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MainCameraComponent : MonoBehaviour
{
    [SerializeField]
    private RefCamera _cameraReference = null;

    private void Awake()
    {
        Camera mainCamera = GetComponent<Camera>();

        _cameraReference.Value = mainCamera;
    }
}
